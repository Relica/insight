<?php

include_once('modele/modele.php');

if (isset($_GET['map'])) {
    include_once('simulator/simulator.php');
} elseif (isset($_GET['page'])) {
    if ($_GET['page'] == 'infos') {
        include_once('infos/infos.php');
    } else {
        header('Location: http://dofus-insight.com');
    }
} else {
    include_once('maps/maps.php');
}
