<?php

function showError($req)
{
    echo "\nPDO::errorInfo():\n";
    print_r($req->errorInfo());
}

function getCategories($bdd)
{
    $req = $bdd->prepare('
        SELECT id, name
        FROM category
        ORDER BY sortIndex ASC
    ');

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function getUsedCategories($bdd)
{
    $req = $bdd->prepare('
        SELECT DISTINCT c.id, c.name
        FROM category c, map m
        WHERE m.idCategory = c.id
        AND m.active = 1
        ORDER BY c.sortIndex ASC
    ');

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function addMap($bdd, $name, $idCategory)
{
    $req = $bdd->prepare('
        INSERT INTO map (name, idCategory)
        VALUES (:name, :idCategory)
    ');

    $req->bindValue('name', $name);
    $req->bindValue('idCategory', $idCategory);

    if ($req->execute()) {
        return $bdd->lastInsertId();
    } else {
        showError($req);

        return false;
    }
}

function getMapsByCategory($bdd, $idCategory)
{
    $req = $bdd->prepare('
        SELECT id, name, active
        FROM map
        WHERE idCategory = :idCategory
        ORDER BY name
    ');

    $req->bindValue('idCategory', $idCategory);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function getActiveMapsByCategory($bdd, $idCategory)
{
    $req = $bdd->prepare('
        SELECT id, name
        FROM map
        WHERE idCategory = :idCategory
        AND active = 1
        ORDER BY name
    ');

    $req->bindValue('idCategory', $idCategory);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function getMapById($bdd, $id)
{
    $req = $bdd->prepare('
        SELECT m.id, m.name as name, c.name as categoryName, m.active
        FROM map m, category c
        WHERE m.idCategory = c.id
        AND m.id = :id
    ');

    $req->bindValue('id', $id);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function getCellsByMapId($bdd, $id)
{
    $req = $bdd->prepare('
        SELECT columnX, lineY, type
        FROM cell
        WHERE idMap = :idMap
        ORDER BY lineY, columnX
    ');

    $req->bindValue('idMap', $id);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function getCell($bdd, $idMap, $lineY, $columnX)
{
    $req = $bdd->prepare('
        SELECT *
        FROM cell
        WHERE idMap = :idMap
        AND lineY = :lineY
        AND columnX = :columnX
    ');

    $req->bindValue('idMap', $idMap);
    $req->bindValue('lineY', $lineY);
    $req->bindValue('columnX', $columnX);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function deleteCell($bdd, $idMap, $lineY, $columnX)
{
    $req = $bdd->prepare('
        DELETE FROM cell
        WHERE idMap = :idMap
        AND lineY = :lineY
        AND columnX = :columnX
    ');

    $req->bindValue('idMap', $idMap);
    $req->bindValue('lineY', $lineY);
    $req->bindValue('columnX', $columnX);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function updateCell($bdd, $idMap, $lineY, $columnX, $type)
{
    $req = $bdd->prepare('
        UPDATE cell
        SET type = :type
        WHERE idMap = :idMap
        AND lineY = :lineY
        AND columnX = :columnX
    ');

    $req->bindValue('type', $type);
    $req->bindValue('idMap', $idMap);
    $req->bindValue('lineY', $lineY);
    $req->bindValue('columnX', $columnX);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function addCell($bdd, $idMap, $lineY, $columnX, $type)
{
    $req = $bdd->prepare('
        INSERT INTO cell (idMap, lineY, columnX, type)
        VALUES (:idMap, :lineY, :columnX, :type)
    ');

    $req->bindValue('idMap', $idMap);
    $req->bindValue('lineY', $lineY);
    $req->bindValue('columnX', $columnX);
    $req->bindValue('type', $type);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}

function updateMapActivation($bdd, $idMap, $active)
{
    $req = $bdd->prepare('
        UPDATE map
        SET active = :active
        WHERE id = :idMap
    ');

    $req->bindValue('active', $active);
    $req->bindValue('idMap', $idMap);

    if (!$req->execute()) {
        showError($req);
    }

    return $req;
}
