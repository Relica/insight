<?php
include_once('infos/inc/header.php');
?>

<div id="content">
    <h2>Fonctionnement</h2>
    <p>Passer la souris sur une case permet de mettre en valeur toutes cases visibles depuis celle-ci, qui correspondent de manière réciproque à toutes les cases ayant une ligne de vue sur elle.</p>
    <p>
        [clic gauche] poser/retirer un obstacle
        <br>
        [clic droit] figer la cible
        <br>
        [ctrl+z]/[ctrl+y] annuler/rétablir la pose d'obstacles
    </p>
</div>

<?php
include_once('infos/inc/footer.php');
?>
