<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= $mapName ?> - Dofus Insight</title>
    <link rel="stylesheet" href="simulator/css/style.css"/>
    <link rel="stylesheet" href="navbar/navbar.css"/>
    <link rel="icon" href="images/target.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>

<?php
include_once('navbar/navbar.php');
?>
