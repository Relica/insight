<?php

$mapId = $_GET['map'];
$fetchCategories = getUsedCategories($bdd);
$categories = [];

while ($category = $fetchCategories->fetch()) {
    $fetchMapList = getMapsByCategory($bdd, $category['id']);
    $mapList = [];

    while ($map = $fetchMapList->fetch()) {
        $mapList[] = ['mapId' => $map['id'], 'mapName' => $map['name']];
    }

    $categories[] = ['categoryName' => $category['name'], 'mapList' => $mapList];
}

$mapObject = getmapById($bdd, $mapId);
$map = $mapObject->fetch();

if ($map == null) {
    $mapName = 'Map not found';
    include_once('inc/header.php');
    echo '<p id="invalid-id">Sorry, this map does not exist yet.</p>';
    die();
}

$mapName = $map['name'];
$categoryName = $map['categoryName'];
$cells = getCellsByMapId($bdd, $map['id']);
$obstacles = [];

while ($cell = $cells->fetch()) {
    $obstacles[] = [(int) $cell['lineY'], (int) $cell['columnX'], (int) $cell['type']];
}

include_once('inc/header.php');
?>

<script>
    const obstacles = <?= json_encode($obstacles) ?>;
</script>

<span id="map-name">
    <?= $categoryName . '&nbsp;&nbsp;&ndash;&nbsp;&nbsp;' . $mapName ?>
</span>
<div id="content">
    <div id="map"></div>
</div>
<div id="target-image-preloader"></div>

<?php
include_once('inc/footer.php');
