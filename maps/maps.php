<?php

$fetchCategories = getUsedCategories($bdd);
$categories = [];

while ($category = $fetchCategories->fetch()) {
    $fetchMapList = getActiveMapsByCategory($bdd, $category['id']);
    $mapList = [];

    while ($map = $fetchMapList->fetch()) {
        $mapList[] = ['mapId' => $map['id'], 'mapName' => $map['name']];
    }

    $categories[] = ['categoryName' => $category['name'], 'mapList' => $mapList];
}

include_once('maps/inc/header.php');
?>

<div id="content">
    <h1>Dofus Insight - Simulateur de lignes de vue pour Dofus</h1>
    <div id="controls">
        <?php
        foreach ($categories as $category) {
            echo '<button type="button" data-filter=".' . str_replace(' ', '', $category['categoryName']) . '">' . $category['categoryName'] . '</button>';
        }
        ?>
    </div>

    <div class="container">
        <?php
        foreach ($categories as $category) {
            foreach ($category['mapList'] as $map) {
        ?>
                <a>
                    <div class="map mix <?= str_replace(' ', '', $category['categoryName']) ?>" data-link="?map=<?= $map['mapId'] ?>">
                        <img src="images/maps/<?= $map['mapId'] ?>.png" class="mapImage" />
                        <div class="mapMeta"><?= $map['mapName'] ?></div>
                    </div>
                </a>
        <?php
            }
        }
        ?>
    </div>
</div>

<script>
    window.onload = function() {
        setTimeout(function() {
            window.scrollTo(0, 0);
        }, 1);
    }

    let containerEl = $('.container');
    let mixer = mixitup(containerEl, {
        load: {
            filter: '.Kolizéum1v1'
        },
        animation: {
            duration: 300,
            nudge: false,
            reverseOut: false,
            effectsIn: 'fade scale(0.25)',
            effectsOut: 'fade',
        }
    });

    const maps = $('.map');

    maps.click(function() {
        maps.addClass('blur');
        currentMap = $(this);
        currentMap.removeClass('blur');
        setTimeout(function() {
            window.location.href = currentMap.attr('data-link');
        }, 400);
    });
</script>

<?php
include_once('maps/inc/footer.php');
?>
