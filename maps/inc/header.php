<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dofus Insight</title>
    <meta name="description" content="Trouvez vos lignes de vue grâce à ce simulateur de ligne de vue pour DOFUS." />
    <link rel="stylesheet" href="maps/css/style.css"/>
    <link rel="stylesheet" href="navbar/navbar.css"/>
    <link rel="icon" href="images/target.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="js/mixitup.min.js"></script>
</head>
<body>

<?php
include_once('navbar/navbar.php');
?>
