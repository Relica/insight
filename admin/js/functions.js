$(function() {
    const map = [];
    let mapAppend = '';
    let cursor = 'bg-wall';
    let isMouseDown = false;

    // pattern pour la création de la map, on commence en haut à droite, offset à gauche + nb cells
    mapPattern = [
        [13, 2],
        [12, 4],
        [11, 6],
        [10, 8],
        [9, 10],
        [8, 12],
        [7, 14],
        [6, 16],
        [5, 18],
        [4, 20],
        [3, 22],
        [2, 24],
        [1, 26],
        [0, 28],
        [1, 28],
        [2, 28],
        [3, 28],
        [4, 28],
        [5, 28],
        [6, 28],
        [7, 26],
        [8, 24],
        [9, 22],
        [10, 20],
        [11, 18],
        [12, 16],
        [13, 14],
        [14, 12],
        [15, 10],
        [16, 8],
        [17, 6],
        [18, 4],
        [19, 2],
    ];
    // maxSize = largeur de la map
    let maxSize = mapPattern[0][0] + mapPattern[0][1];

    for (let i = 1; i < mapPattern.length; i++) {
        currentSize = mapPattern[i][0] + mapPattern[i][1];

        if (currentSize > maxSize) {
            maxSize = currentSize;
        }
    }

    // création de la map js
    for (let line = 0; line < mapPattern.length; line++) {
        let newLine = [];

        for (let column = 0; column < mapPattern[line][0]; column++) {
            newLine.push(3);
        }
        for (let column = mapPattern[line][0]; column < mapPattern[line][0] + mapPattern[line][1]; column++) {
            newLine.push(0);
        }
        for (let column = mapPattern[line][0] + mapPattern[line][1]; column < maxSize; column++) {
            newLine.push(3);
        }

        map.push(newLine);
    }

    for (let i = 0; i < obstacles.length; i++) {
        let y = obstacles[i][0];
        let x = obstacles[i][1];
        let type = obstacles[i][2];

        map[y][x] = type;
    }

    // création de la map html
    for (let line = 0; line < map.length; line++) {
        let newLine = '<div class="map-line">';

        for (let column = 0; column < map[line].length; column++) {
            if (map[line][column] == 0) {
                colorClass = (line + column) % 2 == 0 ? 'default bg-default-light' : 'default bg-default-dark';
            } else if (map[line][column] == 1) {
                colorClass = 'bg-wall';
            } else if (map[line][column] == 2) {
                colorClass = 'bg-hole';
            } else {
                colorClass = 'bg-outmap';
            }

            newLine += `
                <div class="square-box ${colorClass}" id="cell-${line}-${column}">
                    <img src="images/wall.png">
                    <div class="filter"></div>
                </div>
            `;
        }

        newLine += '</div>';
        mapAppend += newLine;
    }

    $('#map').append(mapAppend);

    $('#btn-wall').addClass('selected');

    if (stateAction == 'activate') $('#activation > button > span').append('Publier');
    else $('#activation > button > span').append('Dépublier');

    // FIN INITIALISATION

    if ($.inArray(cursor, ['default', 'bg-wall', 'bg-hole']) != -1) {
        $('.square-box').hover(
            function() {
                idSplit = $(this).attr('id').split('-');
                currentLineY = parseInt(idSplit[1]);
                currentColumnX = parseInt(idSplit[2]);
                typeId = map[currentLineY][currentColumnX];

                if ($.inArray(typeId, [0, 1, 2]) != -1) {
                    $(this).addClass('cell-hover');
                }
            },

            function() {
                if ($.inArray(typeId, [0, 1, 2]) != -1) {
                    $(this).removeClass('cell-hover');
                }
            }
        );

        $('.square-box').hover(
            function() {
                if (isMouseDown) {
                    changeCell($(this));
                }
            },

            function() {
                $(this).stop();
            }
        );

        $('.square-box').mousedown(function(e) {
            if (e.which === 1) {
                changeCell($(this));
            }
        });
    }

    $('#btn-default').click(function() {
        cursor = 'default';
        $('#tools button').removeClass('selected');
        $(this).addClass('selected');
    });

    $('#btn-wall').click(function() {
        cursor = 'bg-wall';
        $('#tools button').removeClass('selected');
        $(this).addClass('selected');
    });

    $('#btn-hole').click(function() {
        cursor = 'bg-hole';
        $('#tools button').removeClass('selected');
        $(this).addClass('selected');
    });

    $('#activation').click(function() {
        if (stateAction == 'activate') {
            newState = 'unactivate';
            newStateId = 1;
        } else {
            newState = 'activate';
            newStateId = 0;
        }

        $.get('js/ajax/update-activation.php', {
            idMap: idMap,
            active: newStateId,
        }).done(function() {
            stateAction = newState;
            $('#activation > button > span').text(stateAction == 'activate' ? 'Publier' : 'Dépublier');
        });
    });

    $('body').mousedown(function() {
        isMouseDown = true;
    })
    .mouseup(function() {
        isMouseDown = false;
    });

    function changeCell($cell) {
        idSplit = $cell.attr('id').split('-');
        currentLineY = parseInt(idSplit[1]);
        currentColumnX = parseInt(idSplit[2]);
        typeId = map[currentLineY][currentColumnX];

        if (typeId != 3) {
            if ($cell.hasClass('default')) {
                if ($cell.hasClass('bg-default-light')) {
                    $cell.removeClass('bg-default-light');
                } else if ($cell.hasClass('bg-default-dark')) {
                    $cell.removeClass('bg-default-dark');
                }

                $cell.removeClass('default');
            } else if ($cell.hasClass('bg-wall')) {
                $cell.removeClass('bg-wall');
            } else if ($cell.hasClass('bg-hole')) {
                $cell.removeClass('bg-hole');
            }

            if (cursor == 'default') {
                newType = 0;
                newClass =
                    (currentColumnX + currentLineY) % 2 == 0 ? 'default bg-default-light' : 'default bg-default-dark';
            } else if (cursor == 'bg-wall') {
                newType = 1;
                newClass = cursor;
            } else {
                newType = 2;
                newClass = cursor;
            }

            map[currentLineY][currentColumnX] = newType;

            $.get('js/ajax/update-cell.php', {
                idMap: idMap,
                lineY: currentLineY,
                columnX: currentColumnX,
                newType: newType,
            });

            $cell.removeClass('cell-hover');
            $cell.addClass(newClass);
        }
    }
});
