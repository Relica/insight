<?php

include_once('../../../modele/modele.php');

if (isset($_GET['idMap']) && isset($_GET['lineY']) && isset($_GET['columnX']) && isset($_GET['newType'])) {
    $idMap = $_GET['idMap'];
    $lineY = $_GET['lineY'];
    $columnX = $_GET['columnX'];
    $newType = $_GET['newType'];

    $cell = getCell($bdd, $idMap, $lineY, $columnX)->fetch();

    if ($newType == 0) {
        if ($cell) {
            deleteCell($bdd, $idMap, $lineY, $columnX);
        }
    } else {
        if ($cell) {
            updateCell($bdd, $idMap, $lineY, $columnX, $newType);
        } else {
            addCell($bdd, $idMap, $lineY, $columnX, $newType);
        }
    }
}
