<?php
$categories = getCategories($bdd);
?>

<div id="navbar">
    <ul>
        <li>
            <a href=".">Nouvelle carte</a>
        </li>
        <?php
        while ($category = $categories->fetch()) {
            $mapList = getMapsByCategory($bdd, $category['id']);
        ?>
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn"><?= $category['name'] ?></a>
                <div class="dropdown-content">
                    <?php
                    while ($map = $mapList->fetch()) {
                        $mapId = $map['id'];
                        $mapName = $map['name'];

                        if ($map['active'] == 0) {
                            echo '<a href="map-creator.php?map=' . $mapId . '">' . $mapName . '<span style="color: red;"> off</span></a>';
                        } else {
                            echo '<a href="map-creator.php?map=' . $mapId . '">' . $mapName . '</a>';
                        }
                    }
                    ?>
                </div>
            </li>
        <?php
        }
        ?>
    </ul>
</div>
