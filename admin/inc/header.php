<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>Map editor - Dofus Insight</title>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>
    <link rel="stylesheet" href="css/tools-style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="js/functions.js"></script>
</head>
<body>

<?php
include('navbar.php');
?>
