<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Admin - Dofus Insight</title>
    <link rel="stylesheet" href="css/new-map.css" />
    <link rel="stylesheet" href="css/navbar.css" />
</head>

<body>
    <?php
    include_once('../modele/modele.php');

    if (isset($_GET['name']) && isset($_GET['category'])) {
        $id = addMap($bdd, $_GET['name'], $_GET['category']);

        if ($id) {
            header('Location: map-creator.php?map=' . $id);
        }
    }

    include_once('inc/navbar.php');

    $categories = getCategories($bdd);
    ?>

    <div id="content">
        <h3>Ajouter une carte</h3>
        <form action="" method="get">
            <ul>
                <li>
                    <label>Nom</label>
                    <input type="text" name="name" id="name" minlength="2" maxlength="30" required>
                </li>
                <li>
                    <label>Catégorie</label>&nbsp;
                    <select name="category">
                        <?php
                        while ($category = $categories->fetch()) {
                            echo '<option value="' . $category['id'] . '">' . $category['name'] . '</option>';
                        }
                        ?>
                    </select>
                </li>
                <li>
                    <input type="submit" value="Valider">
                </li>
            </ul>
        </form>
    </div>
</body>

</html>
