<?php

include_once('../modele/modele.php');
include('inc/header.php');

if (!isset($_GET['map'])) {
    header('Location: index.php');
}

$mapId = $_GET['map'];

$mapObject = getmapById($bdd, $mapId);
$map = $mapObject->fetch();

if ($map == null) {
    echo '<p id="invalid-id">Sorry, this map does not exist yet.</p>';
    die();
}

$cells = getCellsByMapId($bdd, $map['id']);
$obstacles = [];

while ($cell = $cells->fetch()) {
    $obstacles[] = [(int) $cell['lineY'], (int) $cell['columnX'], (int) $cell['type']];
}
?>

<script type="text/javascript">
    const idMap = '<?= $map['id']; ?>';
    const mapName = '<?= $map['name']; ?>';
    const obstacles = <?= json_encode($obstacles) ?>;
</script>


<div id="content">
    <div id="map"></div>
</div>

<div id="tools">
    <h3>
        <?= $map['name'] ?>
    </h3>

    <ul>
        <li>
            <div>
                <button class="btn" id="btn-wall">mur</button>
            </div>
        </li>
        <li>
            <div>
                <button class="btn" id="btn-default">sol</button>
            </div>
        </li>
        <li>
            <div>
                <button class="btn" id="btn-hole">trou</button>
            </div>
        </li>
    </ul>

    <div id="activation">
        <button id="btn-activation">
            <span></span>
        </button>
    </div>

    <br>
    <div>/!\ modifs temps réel</div>
</div>

<script>
    let stateAction = '<?= ($map['active']) == '0' ? 'activate' : 'unactivate' ?>';
</script>

<?php
include('inc/footer.php');
?>
